'use strict';

const accountSid = 'ACf87c9bcbed242b5ec2d0854a9516e0e1';
const authToken = 'ae8a82beba333de2a7f9e2c712150aa0';
const twilioClient = require('twilio')(accountSid, authToken);

const esrever = require('esrever');

exports.handler = function(event, context, callback) {
  twilioClient.messages
    .create({
       body: esrever.reverse(event.Message),
       from: '+420736350836', // TODO as config
       to: event.From,
     })
    .then(message => {
      console.log(message);

      var response = {
        statusCode: 200,
        headers: {
          'Content-Type': 'text/html; charset=utf-8'
        },
        body: '<p>form handler, sms sent</p>'
      };
      console.log("will call callback");
      callback(null, response);
      console.log("called callback");
    });
    console.log("after twilio");
};
