variable "twilio_account_sid" {
  type = string
}

variable "twilio_auth_token" {
  type = string
}

variable "form_handler_version" {
  type = string
}

variable "sms_handler_version" {
  type = string
}

variable "function_send_sms_version" {
  type = string
}
