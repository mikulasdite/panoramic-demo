resource "aws_s3_bucket" "src" {
  bucket = "lambda.panoramic-demo.dite.pro"
  acl    = "private"

  versioning {
    enabled = true
  }
}
