FORM_HANDLER_VERSION = v1.0.14
SMS_HANDLER_VERSION = v1.0.9
FUNCTION_SEND_SMS_VERSION = v1.0.4

.PHONY: _
_: preflight apply

.PHONY: preflight
preflight:
	terraform fmt -recursive .

.PHONY: apply
apply:
	terraform apply

.PHONY: deploy-frontend
deploy-frontend:
	terraform apply -target module.frontend -auto-approve
	aws s3 --profile panoramic-demo sync --acl=public-read --cache-control max-age=1 frontend/build/ s3://panoramic-demo.dite.pro/
	# TODO properly invalidate cache

.PHONY: deploy-backend-form-handler
deploy-backend-form-handler:
	cd backend/form-handler && zip -r distribution.zip .
	aws s3 --profile panoramic-demo cp backend/form-handler/distribution.zip s3://lambda.panoramic-demo.dite.pro/form-handler/$(FORM_HANDLER_VERSION)/distribution.zip
	echo "form_handler_version = \"$(FORM_HANDLER_VERSION)\"" > form_handler.auto.tfvars
	rm backend/form-handler/distribution.zip
	terraform apply -target module.backend.aws_lambda_function.form-handler -auto-approve

.PHONY: deploy-backend-sms-handler
deploy-backend-sms-handler:
	cd backend/sms-handler && zip -r distribution.zip .
	aws s3 --profile panoramic-demo cp backend/sms-handler/distribution.zip s3://lambda.panoramic-demo.dite.pro/sms-handler/$(SMS_HANDLER_VERSION)/distribution.zip
	echo "sms_handler_version = \"$(SMS_HANDLER_VERSION)\"" > sms_handler.auto.tfvars
	rm backend/sms-handler/distribution.zip
	terraform apply -target module.backend.aws_lambda_function.sms-handler -auto-approve

.PHONY: deploy-backend-function-send-sms
deploy-backend-function-send-sms:
	cd backend/function-send-sms && zip -r distribution.zip .
	aws s3 --profile panoramic-demo cp backend/function-send-sms/distribution.zip s3://lambda.panoramic-demo.dite.pro/function-send-sms/$(FUNCTION_SEND_SMS_VERSION)/distribution.zip
	echo "function_send_sms_version = \"$(FUNCTION_SEND_SMS_VERSION)\"" > function_send_sms.auto.tfvars
	rm backend/function-send-sms/distribution.zip
	terraform apply -target module.backend.aws_lambda_function.function-send-sms -auto-approve
