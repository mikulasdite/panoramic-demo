resource "aws_api_gateway_rest_api" "backend" {
  name        = "backend"
  description = "This is my API for demonstration purposes"
}

resource "aws_api_gateway_deployment" "deployment" {
  depends_on = [
    aws_api_gateway_integration.form-handler,
    aws_api_gateway_integration.sms-handler,
  ]

  rest_api_id = aws_api_gateway_rest_api.backend.id
  stage_name  = "prod"
}
