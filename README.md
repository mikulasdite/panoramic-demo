Demo app
========

Terraform expects AWS profile named `panoramic-demo`. This is by default a `~/.aws/credentials` file with
```
[panoramic-demo]
region=eu-west-1
aws_access_key_id=AKIA2CYYAD6ZNULRUAOK
aws_secret_access_key=...
```

The following terraform variables are also required, best specified in `credentials.auto.tfvars` as:
```
twilio_account_sid = ""
twilio_auth_token = ""
```
