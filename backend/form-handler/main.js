'use strict';

const querystring = require('querystring');

const accountSid = 'ACf87c9bcbed242b5ec2d0854a9516e0e1';
const authToken = 'ae8a82beba333de2a7f9e2c712150aa0';
const twilioClient = require('twilio')(accountSid, authToken);

const redis = require("redis");

function normalizePhone(raw) {
    return raw.replace(/[\s()-]+/gi, "")
}

function phoneToRedisKey(normalized) {
    return "func_id-" + normalized.replace(/[+]+/gi, "")
}

exports.handler = function(event, context, callback) {
  const body = querystring.parse(event.body);

  console.log(body);

  if (! 'phone' in body || body.phone === "") {
    callback(null, {
      success: false,
      error: "Invalid payload, missing 'phone'"
    });
    return;
  }
  if (! 'function_id' in body || body.function_id === "") {
    callback(null, {
      success: false,
      error: "Invalid payload, missing 'function_id'"
    });
    return;
  }

  const phone = normalizePhone(body.phone);
  const redisClient = redis.createClient(6379, "redis.vcyjkt.0001.euw1.cache.amazonaws.com");
  const redisKey = phoneToRedisKey(phone);
  console.log(`saving map to redis key '${redisKey}'`);
  redisClient.set(redisKey, body.function_id, function(err, resp) {
    if (err) {
        console.log("failed to save mapping to redis:", err)
    }
    redisClient.quit(); // required, otherwise request never ends when calling callback
  });

  console.log("sending to twilio");

  let smsText;
  switch (body.function_id) {
      case "function-send-sms":
        smsText = 'Hi, this is an automated message. Please reply and your answer will be reversed:';
        break;
      default:
        smsText = 'Hi, this is an automated message. If you reply, nothing will happen.';
  }

  twilioClient.messages
    .create({
       body: smsText,
       from: '+420736350836', // TODO as config
       to: phone
     })
    .then(message => {
      console.log(message);

      var response = {
        statusCode: 200,
        headers: {
          'Content-Type': 'application/json',
          "Access-Control-Allow-Origin": "https://panoramic-demo.dite.pro",
          "Access-Control-Allow-Headers": "Content-Type",
          "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
        },
        body: JSON.stringify({success: true})
      };
      console.log("will call callback");
      callback(null, response);
      console.log("called callback");
    });
    console.log("after twilio");
};
