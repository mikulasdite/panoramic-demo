<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Panoramic Demo App</title>
  </head>
  <body class="d-flex flex-column h-100">

    <main role="main" class="flex-shrink-0">
      <div class="container">

        <h1>Panoramic Demo App</h1>
        <form action="${backend_url}/form-handler" method="post" id="myForm">
            <div class="form-group">
              <label for="exampleInputEmail1">Phone</label>
              <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="phone" placeholder="+420xxxxxxxxx">
              <small id="emailHelp" class="form-text text-muted">We will send an SMS to this number.</small>
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Function</label>
                <select class="form-control" id="exampleFormControlSelect1" name="function_id">
                  <option value="function-send-sms">Reply via SMS with user's response reversed</option>
                  <option value="function-send-email" disabled>Send a random joke to an email address specified by user</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary" id="submit">Send</button>

            <div class="alert alert-light d-none" role="alert" id="alert-wait">
              Sending…
            </div>
            <div class="alert alert-success d-none" role="alert" id="alert-ok">
              Sent!
            </div>
            <div class="alert alert-danger d-none" role="alert" id="alert-failed">
              Failed!
            </div>
        </form>
      </div>
    </main>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
			  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
			  crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <script>
        $("#myForm").submit(function(e) {
            e.preventDefault(); // avoid to execute the actual submit of the form.
            var form = $(this);
            var url = form.attr('action');

            console.log('beforeSubmit');
            $('#alert-ok').addClass('d-none');
            $('#alert-wait').removeClass('d-none');
            $('#submit').attr("disabled", "disabled").addClass('disabled');

            $.ajax({
                   type: "POST",
                   url: url,
                   data: form.serialize(), // serializes the form's elements.
                   success: function(data) {
                       console.log('success');
                       $('#alert-wait').addClass('d-none');
                       $('#alert-ok').removeClass('d-none');
                       $('#submit').removeAttr("disabled").removeClass('disabled');

                        setTimeout(function(){ $('#alert-ok').addClass('d-none'); }, 5000);
                   }
                 });
        });
    </script>
  </body>
</html>
