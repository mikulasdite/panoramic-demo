variable "form_handler_version" {
  description = "Tag of form-handler code to register with lambda function"
  type        = string
}

variable "sms_handler_version" {
  description = "Tag of sms-handler code to register with lambda function"
  type        = string
}

variable "function_send_sms" {
  description = "Tag of function-send-sms code to register with lambda function"
  type        = string
}

variable "subnet_ids" {
  type = list(string)
}

variable "security_group_ids" {
  type = list(string)
}

