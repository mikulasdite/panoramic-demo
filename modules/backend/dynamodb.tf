resource "aws_dynamodb_table" "dynamodb-table" {
  name         = "Responses"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "From"
  range_key    = "CreatedAt"

  attribute {
    name = "From"
    type = "S"
  }

  attribute {
    name = "CreatedAt"
    type = "S"
  }

  // Used in the documents but not defined in the schema
  //  attribute {
  //    name = "Message"
  //    type = "S"
  //  }
  //
  //  attribute {
  //    name = "FunctionId"
  //    type = "S"
  //  }
}
