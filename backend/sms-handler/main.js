'use strict';

const querystring = require('querystring');
const redis = require("redis");
const AWS = require("aws-sdk");

// Twilio Payload:
//   ToCountry: CZ
//   ToState:
//   SmsMessageSid: SM4472416b4bea36ea971ea0d1f388c7a3
//   NumMedia: 0
//   ToCity:
//   FromZip:
//   SmsSid: SM4472416b4bea36ea971ea0d1f388c7a3
//   FromState:
//   SmsStatus: received
//   FromCity:
//   Body: my text
//   FromCountry: CZ
//   To: +420736350836
//   ToZip:
//   NumSegments: 1
//   MessageSid: SM4472416b4bea36ea971ea0d1f388c7a3
//   AccountSid: ACf87c9bcbed242b5ec2d0854a9516e0e1
//   From: +420721405507
//   ApiVersion: 2010-04-01

function phoneToRedisKey(normalized) {
    return "func_id-" + normalized.replace(/[+]+/gi, "")
}

exports.handler = function(event, context, callback) {
  const body = querystring.parse(event.body);
  console.log(`received text '${body.Body}' from ${body.From}`);

  const redisClient = redis.createClient(6379, "redis.vcyjkt.0001.euw1.cache.amazonaws.com");
  const redisKey = phoneToRedisKey(body.From);
  console.log(`reading from redis key '${redisKey}'`);
  redisClient.get(redisKey, function(err, functionId) {
    if (err) {
      console.log("TODO no function mapping found");
      callback(null, {success: false});
      return
    }

    // TODO rewrite to promises

    console.log(`mapped phone to functionId '${functionId}'`);

    console.log(`TODO persist to dynamodb`);
    const dynamodbClient = new AWS.DynamoDB.DocumentClient();
    const entry = {
      TableName: "Responses",
      Item: {
        From: body.From,
        CreatedAt: (new Date()).toISOString(),
        Message: body.Body,
        FunctionId: functionId,
      }
    };
    dynamodbClient.put(entry, function(err, data) {
      if (err) {
        console.log("failed to persist response to dynamodb");
        callback(null, {success: false});
        return

      } else {
        if (!['function-send-sms'].includes(functionId)) { // TODO
          console.log(`functionId ${functionId} is not whitelisted`);
          callback(null, {success: false});
          return
        }

        console.log(`invoking functionId ${functionId}`);
        var lambda = new AWS.Lambda({
          region: 'eu-west-1'
        });
        lambda.invoke({
          FunctionName: functionId,
          Payload: JSON.stringify(entry.Item),
          // InvocationType: "Event",
          // LogType: "None",
        }, function(err, data) {
          console.log(err, data);
          if (err) {
            context.done('error', err);
          }
          callback(null, {success: true});
        });
      }
    });

    redisClient.del(redisKey);
    redisClient.quit(); // required, otherwise request never ends when calling callback
  });
};
