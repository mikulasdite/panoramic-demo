output "src_bucket" {
  value = aws_s3_bucket.src.bucket
}

output "apigw_url" {
  value = aws_api_gateway_deployment.deployment.invoke_url
}
