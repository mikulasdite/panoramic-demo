resource "aws_lambda_function" "function-send-sms" {
  function_name = "function-send-sms"

  s3_bucket = aws_s3_bucket.src.bucket
  s3_key    = "function-send-sms/${var.function_send_sms}/distribution.zip"

  handler = "main.handler"
  runtime = "nodejs12.x"

  role = aws_iam_role.lambda_exec.arn

  publish = true

  vpc_config {
    security_group_ids = var.security_group_ids
    subnet_ids         = var.subnet_ids
  }
}

resource "aws_cloudwatch_log_group" "function-send-sms" {
  name              = "/aws/lambda/${aws_lambda_function.function-send-sms.function_name}"
  retention_in_days = 1
}
