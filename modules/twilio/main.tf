resource "twilio_phone_number" "main" {
  country_code  = "CZ"
  friendly_name = "+420 736 350 836"

  sms {
    primary_url         = "${var.backend_url}/sms-handler"
    primary_http_method = "POST"
  }
}
