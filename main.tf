terraform {
  required_version = "0.12.20"

  backend "s3" {
    bucket  = "terraform.panoramic-demo.dite.pro"
    key     = "v2.eu-central-1.k8s.mangoweb.org"
    region  = "eu-west-1"
    profile = "panoramic-demo"
  }
}

provider "aws" {
  region  = "eu-west-1"
  profile = "panoramic-demo"
  version = "~> 2.50.0"
}

# required for ACM certs used with CloudFront
provider "aws" {
  alias   = "us-east-1"
  region  = "us-east-1"
  profile = "panoramic-demo"
  version = "~> 2.50.0"
}

provider "twilio" {
  version = "0.1.4"

  account_sid = var.twilio_account_sid
  auth_token  = var.twilio_auth_token
}

provider "local" {
  version = "~> 1.4.0"
}

// created with local state before setting up s3 backend
resource "aws_s3_bucket" "state" {
  bucket = "terraform.panoramic-demo.dite.pro"
  acl    = "private"
}

module "twilio" {
  source      = "./modules/twilio"
  backend_url = module.backend.apigw_url
}

# defined here because aws.us-east-1 is hard to use in module
resource "aws_acm_certificate" "cert" {
  provider          = aws.us-east-1 # required for ACM certs used with CloudFront
  private_key       = file("${path.root}/certs/privkey1.pem")
  certificate_body  = file("${path.root}/certs/cert1.pem")
  certificate_chain = file("${path.root}/certs/chain1.pem")
}

module "website" {
  source              = "./modules/website"
  acm_certificate_arn = aws_acm_certificate.cert.arn
}

module "backend" {
  source               = "./modules/backend"
  form_handler_version = var.form_handler_version
  sms_handler_version  = var.sms_handler_version
  function_send_sms    = var.function_send_sms_version
  subnet_ids           = ["subnet-2a462370"] # TODO import, has NAT
  security_group_ids   = ["sg-d99aca94"]     # TODO import
}

module "frontend" {
  source      = "./modules/frontend"
  backend_url = module.backend.apigw_url
}
