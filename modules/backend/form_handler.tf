resource "aws_lambda_function" "form-handler" {
  function_name = "form-handler"

  s3_bucket = aws_s3_bucket.src.bucket
  s3_key    = "form-handler/${var.form_handler_version}/distribution.zip"

  handler = "main.handler"
  runtime = "nodejs12.x"

  role = aws_iam_role.lambda_exec.arn

  publish = true

  vpc_config {
    security_group_ids = var.security_group_ids
    subnet_ids         = var.subnet_ids
  }
}

resource "aws_cloudwatch_log_group" "form-handler" {
  name              = "/aws/lambda/${aws_lambda_function.form-handler.function_name}"
  retention_in_days = 1
}

resource "aws_api_gateway_resource" "form-handler" {
  rest_api_id = aws_api_gateway_rest_api.backend.id
  parent_id   = aws_api_gateway_rest_api.backend.root_resource_id
  path_part   = "form-handler"
}

resource "aws_api_gateway_method" "form-handler" {
  rest_api_id   = aws_api_gateway_rest_api.backend.id
  resource_id   = aws_api_gateway_resource.form-handler.id
  http_method   = "ANY"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "form-handler" {
  rest_api_id = aws_api_gateway_rest_api.backend.id
  resource_id = aws_api_gateway_method.form-handler.resource_id
  http_method = aws_api_gateway_method.form-handler.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.form-handler.invoke_arn
}

resource "aws_lambda_permission" "apigw-form-handler" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.form-handler.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_api_gateway_rest_api.backend.execution_arn}/*/*"
}
