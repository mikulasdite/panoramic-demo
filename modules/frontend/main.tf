resource "local_file" "index" {
  filename = "${path.root}/frontend/build/index.html"
  content = templatefile("${path.root}/frontend/index.html.tpl", {
    backend_url = var.backend_url
  })
}
